/// Program that displays checkerboard pattern with eight output statements,
/// then display the same pattern using as few statements as possible.
#include <iostream> /// allow program to perform output and input

/// function main begin program execution
int
main()
{
    /// pattern with eight output statemenst
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * * \n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * * \n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * * \n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * * " << std::endl;

    /// pattern with as few statements as possible
    std::cout << "\n * * * * * * * *\n" << "* * * * * * * * \n" << " * * * * * * * *\n" << "* * * * * * * * \n" << " * * * * * * * *\n" << "* * * * * * * * \n" << " * * * * * * * *\n" << "* * * * * * * * " << std::endl;

    return 0; /// indicate that program finished successfully
} /// end of function main

