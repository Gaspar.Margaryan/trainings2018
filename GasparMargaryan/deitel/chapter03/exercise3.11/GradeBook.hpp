/// Exercise3.11 GradeBook.hpp
/// GradeBook class definition. This file presents GradeBook's public
/// interface without revealing the implementations of GradeBook's member
/// functions, which are defined in GradeBook.cpp.
#include <string> /// class GradeBook uses C++ standard string class

/// GradeBook class definition
class GradeBook
{
public:
    GradeBook(std::string course, std::string instructor); /// constructor that initializes courseName and instructorName    
    void setCourseName(std::string course); /// function that sets the course name
    std::string getCourseName(); /// function that gets the course name
    void setInstructorName(std::string instructor); /// function that sets the instructor name
    std::string getInstructorName(); /// function that gets the instructor name
    void displayMessage(); /// function that displays a welcome message 
private:
    std::string courseName_; /// course name for this GradeBook
    std::string instructorName_; /// instructor's name for this GradeBook
}; /// end class GradeBook

