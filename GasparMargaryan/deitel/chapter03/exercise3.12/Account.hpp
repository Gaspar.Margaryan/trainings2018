/// Exercise3.12 Account.hpp
/// Account class definition
class Account
{
public:
    Account(int balance); /// constructor that initializes accountBalance
    int getAccountBalance(); /// function that gets the account balance
    void credit(int credit);
    void debit(int debit);
private:
    int accountBalance_; /// account balance for this Account
}; /// end class Account

