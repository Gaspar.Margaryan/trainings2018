/// Exercise4_13.cpp
/// Program that calculate the miles per gallon obtained for each tankful
#include <iostream>
#include <iomanip>   /// input output manipulation header for using setprecision () manipulator
#include <unistd.h>

int
main()
{
    int totalMiles = 0;   /// declare total miles as integer and initialize as 0 to prevent "garbage" value error
    int totalGallons = 0;   /// declare total gallons as integer and initialize as 0 to prevent "garbage" value error

    while (true) {  /// loop until inputed -1 to exit
        if (::isatty(STDIN_FILENO)) {
            /// prompt user to input miles for calculation or -1 to exit the program
            std::cout << "Enter the miles used or enter -1 to quit! " << std::endl;
        }
        int miles = 0; /// declare miles as integer
        std::cin >> miles;
        if (-1 == miles) {
            std::cout << "Thank you for using our program \n" << std::endl;
            return 0;
        }
        if (miles <= 0) {
            std::cerr << "Error 1: Invalid miles value! " << std::endl;  /// if miles are smaller than 0 end the program
            return 1;
        }
        if (::isatty(STDIN_FILENO)) {
            /// prompts user to input gallons for current mileage
            std::cout << "Enter gallons for current used miles! " << std::endl;
        }
        int gallons = 0;  /// declare gallons as integer
        std::cin >> gallons;
        if (gallons <= 0) {
            std::cerr << "Error 2: Invalid gallons value! " << std::endl;  /// if gallons are smaller than 0 end the program
            return 2;
        }

        /// declare miles per gallon as double floating point and calculate it to get floating point result from integer variables
        double milesPerGallon = static_cast<double>(miles) / gallons;

        /// after each legal input prints current mile per gallon calculation with set 6 digit numeric precision after decimal point
        std::cout << "Current MPG for this tankful: " << std::setprecision (6) << std::fixed << milesPerGallon << std::endl;
        
        totalMiles += miles;
        totalGallons += gallons;

        /// calculate total miles per gallon and get floating point result from integer variables
        double totalMilesPerGallon = static_cast<double>(totalMiles) / totalGallons;

        /// prints total mile per gallon calculation with set 6 digit numeric precision after decimal point
        std::cout << "Total MPG: " << std::setprecision (6) << std::fixed << totalMilesPerGallon << std::endl;
    }   /// end the while loop

    return 0; /// indicate successful termination
} /// end main

