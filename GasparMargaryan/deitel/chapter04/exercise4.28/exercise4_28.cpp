/// Exercise 4.28
/// Program that input a number for a size of checkerboard and print it
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Please enter a number for the size of checker board from 1 to 50 " << std::endl;
    }
    int size; /// declare size of checkerboard
    std::cin >> size;  /// input
    if (size <= 0) {
        std::cout << "Error 1: Invalid number. The size should be positive number. " << std::endl;
        return 1;
    }   ///  print out error message when entered wrong size number
    if (size > 50) {
        std::cout << "Error 2: Invalid number. " << std::endl;
        return 2;
    }   ///  print out error message when entered wrong size number
    int column = size;
    while (column >= 1) { /// loop until column >= 1
        if (column % 2) {
            std::cout << ' ';  /// print white space at the beginning if column % 2
        }
        int row = 1;  /// assign 1 to row for loop
        while (row <= size) { /// loop n times
            std::cout << "* ";
            ++row; /// increment column
        } /// end inner while
        --column; /// decrement row
        std::cout << std::endl; // begin new output line
    } /// end while
    return 0; /// indicate successful termination
} /// end main

