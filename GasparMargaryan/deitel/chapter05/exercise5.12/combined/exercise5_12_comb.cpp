/// Exercise5_12
/// Program that prints patterns combined
#include <iostream>

int
main()
{
    std::cout << "a) " << "\t\t" << "b) " << "\t\t" << "c) " << "\t\t" << "d) \n" << std::endl;
    for (int row = 1; row <= 10; ++row) {
        /// pattern a)
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        }
        for (int space = 1; space <= 10 - row; ++space) {
            std::cout << " ";
        }
        std::cout << "\t";

        /// pattern b)
        for (int column = 10; column >= row; --column) {
            std::cout << "*";
        }
        for (int space = 1; space < row; ++space) {
            std::cout << " ";
        }
        std::cout << "\t";

        /// pattern c)
        for (int space = 1; space < row; ++space) {
            std::cout << " ";
        }
        for (int column = 10; column >= row; --column) {
            std::cout << "*";
        }
        std::cout << "\t";

        /// pattern d)
        for (int space = 1; space <= 10 - row; ++space) {
            std::cout << " ";
        }
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        }
        std::cout << "\n";
    }

    return 0; /// indicate successful termination
} /// end main

