/// Exercise5_06
/// Program that calculates and print the average of several integers
#include <iostream>
#include <unistd.h>

int
main()
{
    int total = 0;
    int count = 0;
    while (true) {
        if (::isatty(STDIN_FILENO)) { /// prompting user to input value
            std::cout << "Enter a value to process or 9999 to end the program: " << std::endl;
        }
        int value;
        std::cin >> value;
        if (9999 == value) {  /// stops the while loop when entered 9999
            break;
        }
        total += value;
        ++count;
    }
    if (0 == count) { /// print about when nothing is input
        std::cout << "Nothing is input. Exiting... " << std::endl;
        return 0; /// indicate succesful termination without a process
    }
    std::cout << "The average value is " << total / count << std::endl;

    return 0; /// indicate successful termination
} /// end main

