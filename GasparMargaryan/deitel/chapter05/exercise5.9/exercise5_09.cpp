/// Exercise5_09
/// Program that prints the product of odd numbers from 1 to 15
#include <iostream>

int
main()
{
    int product = 1;
    for (int counter = 1; counter <= 15; counter += 2) {
        product *= counter;
    }
    std::cout << "The product of odd numbers from 1 to 15 is " << product << std::endl;

    return 0; /// indicate successful termination
} /// end main

