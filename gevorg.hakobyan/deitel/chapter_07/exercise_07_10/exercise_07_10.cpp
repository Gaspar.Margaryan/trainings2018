#include <iostream>

int
main() 
{
    const int WORKERS_COUNT   = 20;
    const int FREQUENCY_COUNT = 9;
    int workersSales[WORKERS_COUNT] = {0};
    int frequencyArray[FREQUENCY_COUNT] = {0};

    for (int i = 0; i < WORKERS_COUNT; ++i) {
        std::cout << "Worker " << i + 1 << " sales sum: ";
        std::cin  >> workersSales[i];
    }

    for (int i = 0; i < WORKERS_COUNT; ++i) {
        const int workerSalaryIndex = (workersSales[i] * 0.09 + 200) / 100;
        const int frequencyIndex = (workerSalaryIndex - 2 >= FREQUENCY_COUNT ? 8 : workerSalaryIndex - 2); 
        frequencyArray[frequencyIndex]++;
    }

    for (int i = 0; i < FREQUENCY_COUNT - 1; ++i) {
        const int salarySizeStart = (i + 2) * 100;
        std::cout << i + 1 << ". $" << salarySizeStart;
        std::cout << " - $" << salarySizeStart + 99;
        std::cout << ": " << frequencyArray[i] << std::endl;
    }
    std::cout << "9. $1000 and above: " << frequencyArray[FREQUENCY_COUNT - 1] << std::endl;

    return 0;
}

