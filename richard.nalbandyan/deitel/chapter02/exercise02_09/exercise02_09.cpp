#include <iostream>

int
main()
{
    int a, b, c;

    std::cout << "Enter Two Numbers \n"; /// Prints Message On The Screen 
    std::cin >> a >> b >> c; /// Assigns Values To The Integers

    a = b * c;
    std::cout << a; /// Prints Out The Value of a
    return 0;
}