2.14 Given the algebraic equation y = ax^3 + 7, which of the following, if any, are correct C++ statements for this equation?


a. y = a * x * x * x + 7; /// Yes
b. y = a * x * x * ( x + 7); /// No
c. y = ( a * x ) * x * ( x + 7); /// No
d. y = (a * x) * x * x + 7; /// Yes
e. y = a * ( x * x * x ) + 7; /// Yes
f. y = a * x * ( x * x + 7); /// No
