#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Insert two numbers: ";
    std::cin >> number1 >> number2;

    if (0 == number2) {
        std::cout << "Error 1: The first CANNOT be a multiple of the second (0)." << std::endl;
        return 1;
    }

    int remainder = number1 % number2;
    if (0 != remainder) {
        std::cout << "The first is NOT a multiple of the second." << std::endl;
        return 0;
    }

    std::cout << "The first is a multiple of the second." << std::endl;
    return 0;
}

