#include "GradeBook.hpp"
#include <iostream>

int
main()
{
    GradeBook gradeBook("CS101 Introduction to C++ Programming", "Mr. Robins");
    std::cout << "gradeBook instructor name is: " << gradeBook.getInstructorName() << "\n\n";
    gradeBook.setInstructorName("Mr. Johnson");
    std::cout << "new gradeBook instructor name is: " << gradeBook.getInstructorName() << "\n\n";
    gradeBook.displayMessage();
    return 0;
}

