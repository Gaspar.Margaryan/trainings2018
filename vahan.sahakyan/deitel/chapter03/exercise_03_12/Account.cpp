#include "Account.hpp"
#include <iostream>

Account::Account(int balance)
{
    if (balance < 0) {
        std::cerr << "Warning 1: Invalid Balance\n";
        accountBalance_ = 0;
        return;
    }
    accountBalance_ = balance;
}

void
Account::credit(int credit)
{
    accountBalance_ = accountBalance_ + credit;
}

void
Account::debit(int debit)
{
    if (debit > accountBalance_) {
        std::cerr << "\nWarning 2: Debit amount exceeded account balance." << std::endl;
        return;
    }
    accountBalance_ = accountBalance_ - debit;
}

int
Account::getBalance()
{
    return accountBalance_;
}

