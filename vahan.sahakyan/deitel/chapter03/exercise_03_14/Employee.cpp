#include "Employee.hpp"
#include <iostream>

Employee::Employee(std::string firstName, std::string lastName, int monthlySalary)
{
    setFirstName(firstName);
    setLastName(lastName);
    setMonthlySalary(monthlySalary);
}

void
Employee::setFirstName(std::string firstName)
{
    firstName_ = firstName;
}

std::string
Employee::getFirstName()
{
    return firstName_;
}

void
Employee::setLastName(std::string lastName)
{
    lastName_ = lastName;
}

std::string
Employee::getLastName()
{
    return lastName_;
}

void
Employee::setMonthlySalary(int monthlySalary)
{
    if (monthlySalary < 0) {
        std::cerr << "WARNING 1: Monthly salary cannot be negative. Monthly salary set to 0\n";
        monthlySalary_ = 0;
        return;
    }
    monthlySalary_ = monthlySalary;
}

int
Employee::getMonthlySalary()
{
    return monthlySalary_;
}

int
Employee::getYearlySalary()
{
    return getMonthlySalary() * 12;
}

