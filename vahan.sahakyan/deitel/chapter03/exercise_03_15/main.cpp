#include "Date.hpp"
#include <iostream>

int
main()
{
    Date date(7, 3, 1993);

    std::cout << "Month: " << date.getMonth() << std::endl;
    std::cout << "Day: " << date.getDay() << std::endl;
    std::cout << "Year: " << date.getYear() << std::endl;

    std::cout << "\nOld Date:" << std::endl;
    date.displayDate();

    std::cout << "\nThe date has been modified!" << std::endl;
    date.setMonth(13); /// invalid parameter, must print the warning message.
    date.setDay(27);
    date.setYear(2018);

    std::cout << "\nNew Date:" << std::endl;
    date.displayDate();
    return 0;
}

