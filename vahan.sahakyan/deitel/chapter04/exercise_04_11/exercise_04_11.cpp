/// a)
if (age >= 65); /// should not be any semicolon here.
    cout << "Age is greater than or equal to 65" << endl;
else
    cout << "Age is less than 65 << endl"; /// the closing d.q. must be placed after 65 before the "<<" operator.

/// b)
if (age >= 65)
    cout << "Age is greater than or equal to 65" << endl;
else; /// should not be any semicolon here.
    cout << "Age is less htan 65 << endl"; /// the closing d.q. must be placed after 65 before the "<<" operator.

/// c)
int x = 1, total; ///total is a "Magic Number", must be initialized to 0.

while (x <= 10)
{
    total += x;
    ++x;
}

/// d)
While (x <= 100) /// 'W' should be lowercase.
    total += x;
    ++x; /// the body of "while" must start and end with curly brackets.

/// e)
while (y > 0)
{
    cout << y << endl;
    ++y;
} /// this loop is infinite. the 'y' must be decremented.
