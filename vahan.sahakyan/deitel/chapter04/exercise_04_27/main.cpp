#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter a binary number: ";
    }
    int number, decimal = 0, counter = 1;
    std::cin >> number;

    while (number != 0) {
        int digit = number % 10;
        if (digit != 0 && digit != 1) {
            std::cerr << "ERROR 1: This number is not binary!\n";
            return 1;
        }

        decimal += digit * counter;
        counter *= 2;
        number /= 10;
    }

    std::cout << "The decimal equivalent: " << decimal << std::endl;
    return 0;
}

