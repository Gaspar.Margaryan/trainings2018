#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter three side sizes, to check if they are triangle sides...\n" << std::endl;
    }

    double side1, side2, side3; /// sides' declaration

    if (::isatty(STDIN_FILENO)) {
        std::cout << "1st side: ";
    }
    std::cin >> side1;
    if (side1 <= 0) {
        std::cerr << "\nERROR 1: The sides must be positive.\n";
        return 1;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "2nd side: ";
    }
    std::cin >> side2;
    if (side2 <= 0) {
        std::cerr << "\nERROR 1: The sides must be positive.\n";
        return 1;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "3rd side: ";
    }
    std::cin >> side3;
    if (side3 <= 0) {
        std::cerr << "\nERROR 1: The sides must be positive.\n";
        return 1;
    }

    if (side1 + side2 > side3) {
        if (side1 + side3 > side2) {
            if (side2 + side3 > side1) {
                std::cout << "\nResult: Correct! These are triangle sides!" << std::endl;
                return 0;
            }
        }
    }

    std::cout << "\nResult: Wrong! These are NOT triangle sides!" << std::endl;
    return 0;
}

