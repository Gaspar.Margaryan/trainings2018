/// A program that calculates the diameter, circumference and area of a circle.

#include <iostream> /// allows the program to input and output data

/// function main begins program execution

int
main()
{
    int radius;
    std::cout << "Please enter the circle's radius: ";
    std::cin >> radius;
    
    if (radius < 0) {
        std::cout << "Error 1: the radius cannot be negative!" << std::endl;
	return 1;
    }

    std::cout << "Diameter is " << radius * 2 << std::endl;
    std::cout << "Circumference is " << 2 * 3.14159 * radius << std::endl;
    std::cout << "Area is " << 3.14159 * (radius * radius) << std::endl;

    return 0; /// indicates the successful completion of the program
} /// end of function main

