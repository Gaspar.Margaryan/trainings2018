/// A program that retrieves the largest and the smallest integers from the range of 5 integers.

#include <iostream> /// allows the program to input and output data

/// function main begins program execution

int
main()
{
    int number1, number2, number3, number4, number5; /// variables where user-provided numbers will be stored
    std::cout << "Enter 5 integers: "; /// prompts for input
    std::cin >> number1 >> number2 >> number3 >> number4 >> number5; /// reads 5 numbers 
    
    int smallest = number1; /// suppose number1 is the smallest one
    if (number2 < smallest) {
        smallest = number2;
    }
    if (number3 < smallest) {
        smallest = number3;
    }
    if (number4 < smallest) {
        smallest = number4;
    }
    if (number5 < smallest) {
        smallest = number5;
    }

    std::cout << "The smallest number is " << smallest << std::endl;


    int largest = number1; /// suppose number1 is the largest one
    if (number2 > largest) {
        largest = number2;
    }
    if (number3 > largest) {
        largest = number3;
    }
    if (number4 > largest) {
        largest = number4;
    }
    if (number5 > largest) {
        largest = number5;
    }

    std::cout << "The largest number is " << largest << std::endl; 


    return 0; /// indicates the successful completion of the program
} /// end of function main

