#include "Account.hpp"
#include <iostream>

Account::Account(int initialBalance)
{
    if (initialBalance < 0) {
        accountBalance_ = 0;
	std::cout << "Warning 1: The initial balanace is invalid\n";
	return;
    }       
    accountBalance_ = initialBalance;
}

void
Account::credit(int amount)
{
    accountBalance_ += amount;
}

void
Account::debit(int money)
{
    if (money > accountBalance_) {
        std::cout << "Warning 2: Debit amount exceeded account balance.\n";
	return;
    }
    accountBalance_ -= money;    
}

int
Account::getBalance()
{
    return accountBalance_;
}

