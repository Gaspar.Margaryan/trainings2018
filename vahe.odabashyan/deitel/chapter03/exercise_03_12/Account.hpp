#include <iostream>

class Account
{
public:
    Account(int initialBalance);
    void credit(int amount);
    void debit(int money);
    int getBalance();

private:
    int accountBalance_;
};

