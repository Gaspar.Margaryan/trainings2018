#include "Account.hpp"
#include <iostream>

int
main()
{
    Account account1(5000);
    Account account2(2000);

    std::cout << "The initial balance of account1 is " << account1.getBalance() << "USD\n" << std::endl; // return the initial balance of account1
    account1.credit(4000); // add $4000 to account1
    std::cout << "The balance after the account replenishment is " << account1.getBalance() << "USD\n" << std::endl; // return the balance of account1 after the transaction
    account1.debit(10000); // withdraw $10000 from account1
    std::cout << "The balance after the money withdrawal is " << account1.getBalance() << "USD\n" << std::endl; // return the balance of account1 after the transaction
    std::cout << std::endl;

    std::cout << "The initial balance of account2 is " << account2.getBalance() << "USD\n" << std::endl; // return the initial balance of account2
    account2.credit(3000); // add $3000 to account2
    std::cout << "The balance after the account replenishment is " << account2.getBalance() << "USD\n" << std::endl; // return the balance of account2 after the transaction
    account2.debit(4000); // withdraw $4000 from account2
    std::cout << "TThe balance after the money withdrawal is " << account2.getBalance() << "USD\n" << std::endl; // return the balance of account2 after the transaction

    return 0;
}

