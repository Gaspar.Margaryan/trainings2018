#include "Invoice.hpp"
#include <iostream>

int
main()
{
    Invoice invoice1("A125647H", "MSI GS65 Gaming Laptop", 2, 2500);
    std::cout << "\n********INVOICE 1********\n" << std::endl;
    std::cout << "Product part number: " << invoice1.getPartNumber() << std::endl;
    std::cout << "Product description: " << invoice1.getPartDescription() << std::endl;
    std::cout << "Quantity: " << invoice1.getQuantity() << std::endl;
    std::cout << "Unit Price: " << invoice1.getUnitPrice() << std::endl;
    std::cout << "Total: " << invoice1.getInvoiceAmount() << std::endl;
    
    std::cout << std::endl;

    Invoice invoice2("L15648IH", "Samsung Smart TV", -2, 1000);
    std::cout << "\n********INVOICE 2********\n" << std::endl;
    std::cout << "Product part number: " << invoice2.getPartNumber() << std::endl;
    std::cout << "Product description: " << invoice2.getPartDescription() << std::endl;
    std::cout << "Quantity: " << invoice2.getQuantity() << std::endl;
    std::cout << "Unit Price: " << invoice2.getUnitPrice() << std::endl;
    std::cout << "Total: " << invoice2.getInvoiceAmount() << std::endl;

    std::cout << std::endl;

    Invoice invoice3("GP158799", "Google Pixel XL 3", 1, -900);
    std::cout << "\n********INVOICE 3********\n" << std::endl;
    std::cout << "Product part number: " << invoice3.getPartNumber() << std::endl;
    std::cout << "Product description: " << invoice3.getPartDescription() << std::endl;
    std::cout << "Quantity: " << invoice3.getQuantity() << std::endl;
    std::cout << "Unit Price: " << invoice3.getUnitPrice() << std::endl;
    std::cout << "Total: " << invoice3.getInvoiceAmount() << std::endl;
    return 0;
}

