#include "Date.hpp"
#include <iostream>

int
main()
{
    Date date1(12, 1, 2019);
    date1.setMonth(55);
    date1.setDay(120);
    date1.setYear(-2000);
    std::cout << "The date in US format is: ";
    date1.displayDate();
    std::cout << std::endl;

    Date date2(12, 31, 2019);
    std::cout << "The date in Armenian format is: " << date2.getDay() << "/" << date2.getMonth() << "/" << date2.getYear() << std::endl;
    std::cout << "The date in US format is: ";
    date2.displayDate();
    std::cout << std::endl;

    return 0;
}

