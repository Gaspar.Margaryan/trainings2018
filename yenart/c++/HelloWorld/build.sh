echo "Pre-processing..."
g++ -E main.cpp -o main.ii || { echo "Error 1: Pre-processing failed!" && exit 1; }
echo "Done"

echo "Assembling..."
g++ -S main.ii  -o main.s || { echo "Error 2: Assembling failed!" && exit 2; }
echo "Done"

echo "Compiling..."
g++ -c main.s   -o main.o || { echo "Error 3: Compiling failed!" && exit 3; }
echo "Done"

echo "Linking..."
g++    main.o   -o HelloWorld || { echo "Error 4: Linking failed!" && exit 4; }
echo "Done"

echo "HelloWorld" > .gitignore

