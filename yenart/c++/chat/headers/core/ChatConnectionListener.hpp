#ifndef __CHAT_CONNECTION_LISTENER__
#define __CHAT_CONNECTION_LISTENER__

#include <thread>

class ChatServer;

class ChatConnectionListener
{
public:
    ChatConnectionListener(ChatServer* server);
    ~ChatConnectionListener();
private:
    int execute();
private:
    std::thread thread_;
    ChatServer* server_;
    int mainSocket_;
};

#endif /// __CHAT_CONNECTION_LISTENER__

