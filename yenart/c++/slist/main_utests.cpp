#include "slist.hpp"

#include <gtest/gtest.h>

TEST(slist, initial_isEmpty)
{
    slist<int> l;
    EXPECT_TRUE(l.isEmpty());
}

TEST(slist, initial_size_0)
{
    slist<int> l;
    EXPECT_EQ(l.size(), 0);
}

TEST(slist, push_front_1)
{
    slist<int> l;
    l.push_front(5);
    EXPECT_FALSE(l.isEmpty());
    EXPECT_EQ(l.size(), 1);
}

TEST(slist, push_front_2)
{
    slist<int> l;
    l.push_front(5);
    l.push_front(5);
    EXPECT_FALSE(l.isEmpty());
    EXPECT_EQ(l.size(), 2);
    unsigned int size = 0;
    for (slist<int>::const_iterator it = l.begin(); it != l.end(); ++it, ++size) {
        EXPECT_EQ(*it, 5);
    }
    EXPECT_EQ(l.size(), size);
}

