#include <benchmark/benchmark.h>

template<typename FV>
static void
BM_function_runner(benchmark::State& state) {
    FV fv;
    while (state.KeepRunning()) {
	benchmark::DoNotOptimize(fv.find_pyth_triples());
    }
}

struct V1 {
    static int
    find_pyth_triples()
    {
	int count = 0;
	for (int firstSide = 1; firstSide <= 500; ++firstSide) {
	    for (int secondSide = 1; secondSide <= 500; ++secondSide) {
		for (int hypotenuse = 1; hypotenuse <= 500; ++hypotenuse) {
		    if (hypotenuse * hypotenuse == firstSide * firstSide + secondSide * secondSide) {
			++count;
		    }
		}
	    }
	}
	return count;
    }
};

struct V2 {
    static int
    find_pyth_triples()
    {
	const int MAX_SIDE = 1200;
	int count = 0;
	for (int firstSide = 3; firstSide <= MAX_SIDE; ++firstSide) {
            const int firstSideSquare = firstSide * firstSide;
	    for (int secondSide = firstSide + 1; secondSide <= MAX_SIDE; ++secondSide) {
		const int secondSideSquare = secondSide * secondSide;
		const int squaresSum = firstSideSquare + secondSideSquare;
                const int maxHypotenuse = std::min(MAX_SIDE, firstSide + secondSide);
		for (int hypotenuse = secondSide + 1; hypotenuse <= maxHypotenuse; ++hypotenuse) {
		    if (hypotenuse * hypotenuse == squaresSum) {
			++count;
		    }
		}
	    }
	}
	return count;
    }
};

// Register the function as a benchmark
BENCHMARK_TEMPLATE1(BM_function_runner, V1);
BENCHMARK_TEMPLATE1(BM_function_runner, V2);

BENCHMARK_MAIN();

