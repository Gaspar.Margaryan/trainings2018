#include "SomeClass.hpp"

#include <iostream>

class AnotherObject
{
public:
    AnotherObject() { /*std::cout << "AnotherObject" << std::endl;*/ }
    AnotherObject(const int x) { std::cout << "AnotherObject(int)" << std::endl; }
    AnotherObject(const double y) { std::cout << "AnotherObject(double)" << std::endl; }
};

class Object
{
    friend class FriendObject;
public:
    static void f() { std::cout << CONST << std::endl;}
    static const int CONST = 9;
public:
    Object(const int x = 6) : another_(), nonConst_(5), const_(x) {}
    int getNonConst() const { return this->nonConst_; }
    int getConst()    const { return const_; }
    Object& setX(const int x) { nonConst_ += 100 * x; return *this; }
    Object& setY(const int y) { nonConst_ += 1000 * y; return *this; }
private:
    AnotherObject another_;
    int nonConst_;
    const int const_;
};

class FriendObject
{
public:
    void someFunction(Object& object) { object.nonConst_ = 999; }
};

class NonFriendObject
{
public:
    ///void someFunction(Object& object) { object.nonConst_ = 888; }
};

void
f()
{
    Object* v = new Object[100];
    ///std::cout << "The address of the pointer is " << &v << std::endl;
    ///std::cout << "The pointer points to " << v << std::endl;
    ///std::cout << "The value stored in the pointed address is " << v[9].getConst() << std::endl;
    delete [] v;
}

int
main()
{
    SomeClass o;
    o.someFunction();
    return 0;
    Object object1, object2;
    object1.getNonConst();
    
    object1.setX(9);
    object1.setY(8);
    object1.setX(9).setY(8);

    int x;
    std::cin >> x;
    Object object(x);
    std::cout << object.getConst() << std::endl;

    FriendObject friendObject;
    friendObject.someFunction(object);

    NonFriendObject nonFriendObject;
    ///nonFriendObject.someFunction(object);

    return 0;
}

